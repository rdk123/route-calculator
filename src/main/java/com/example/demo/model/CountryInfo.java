package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryInfo {
    @JsonProperty("cca3")
    private String countryCode;
    private List<String> borders = new ArrayList<>();
    private String region;
}
