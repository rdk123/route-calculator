package com.example.demo.service

import com.example.demo.exception.BadRequestException
import com.example.demo.exception.NotFoundException
import com.example.demo.model.CountryInfo
import spock.lang.Subject

class RoutesValidatorTest extends spock.lang.Specification {

    @Subject
    RoutesValidator routesValidator

    CountryInfo origin
    CountryInfo destination

    void setup() {
        routesValidator = new RoutesValidator();
        origin = new CountryInfo(countryCode: "POL", borders: ["CZE", "DEU"], region: "EU")
        destination = new CountryInfo(countryCode: "ITA", borders: ["CZE", "AUS"], region: "EU")
    }

    def "Shouldn't throw any exceptions if countries info is valid"() {
        given:
        when:
        routesValidator.validate(origin, destination)
        then:
        noExceptionThrown()
    }

    def "Should throw not found exception if origin is null"() {
        given:
        origin = null
        when:
        routesValidator.validate(origin, destination)
        then:
        thrown(NotFoundException)
    }

    def "Should throw not found exception if destination is null"() {
        given:
        destination = null
        when:
        routesValidator.validate(origin, destination)
        then:
        thrown(NotFoundException)
    }

    def "Should throw bad request exception if origin doesn't have any borders"() {
        given:
        origin.setBorders(Collections.emptyList())
        when:
        routesValidator.validate(origin, destination)
        then:
        thrown(BadRequestException)
    }

    def "Should throw bad request exception if destination doesn't have any borders"() {
        given:
        destination.setBorders(Collections.emptyList())
        when:
        routesValidator.validate(origin, destination)
        then:
        thrown(BadRequestException)
    }

    def "Should throw bad request if origin and destination don't have the same region"() {
        given:
        destination.setRegion("US")
        when:
        routesValidator.validate(origin, destination)
        then:
        thrown(BadRequestException)
    }


}
