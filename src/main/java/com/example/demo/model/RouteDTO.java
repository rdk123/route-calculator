package com.example.demo.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RouteDTO {
    List<String> route = new ArrayList<>();
}
