package com.example.demo.service;

import com.example.demo.model.CountryInfo;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class CountryCache {

    private final CountryRestClient countryRestClient;
    private Map<String, List<CountryInfo>> countriesInfoByRegion;
    private Map<String, CountryInfo> countriesInfoByCountryCode;

    @PostConstruct
    public void init() {
        cacheData();
    }

    @Scheduled(cron = "${country.cache.cron}")
    private void refreshCache() {
        cacheData();
    }

    private void cacheData() {
        List<CountryInfo> countryInfos = countryRestClient.fetchAllData();
        countriesInfoByRegion = countryInfos
                .stream()
                .filter(e -> e.getBorders().size() > 0)
                .collect(Collectors.groupingBy(CountryInfo::getRegion));

        countriesInfoByCountryCode = countryInfos
                .stream()
                .collect(Collectors.toMap(CountryInfo::getCountryCode, e -> e));
    }

    public List<CountryInfo> getCountriesInfoByRegion(String regionKey) {
        log.info("[CountryCache] Try to get countries info by region: [{}]", regionKey);
        return countriesInfoByRegion.get(regionKey);
    }

    public CountryInfo getCountriesInfoByCountryCode(String countryCode) {
        log.info("[CountryCache] Try to get countries info by country code: [{}]", countryCode);
        return countriesInfoByCountryCode.get(countryCode);
    }
}
