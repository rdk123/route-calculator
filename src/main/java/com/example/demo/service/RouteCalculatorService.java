package com.example.demo.service;

import com.example.demo.model.CountryInfo;
import com.example.demo.model.RouteDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RouteCalculatorService {
    private final RoutesValidator routesValidator;
    private final CountryCache countryCache;

    public RouteDTO getBestRoute(String origin, String destination) {
        CountryInfo originCountryInfo = countryCache.getCountriesInfoByCountryCode(origin);
        routesValidator.validate(originCountryInfo, countryCache.getCountriesInfoByCountryCode(destination));
        PathFinderService pathFinderService =
                new PathFinderService(countryCache.getCountriesInfoByRegion(originCountryInfo.getRegion()));

        return pathFinderService.findShortestPath(origin, destination);
    }

}
