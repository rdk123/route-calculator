package com.example.demo.service;

import com.example.demo.model.CountryInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class CountryRestClient {
    private final RestTemplate countryRestTemplate;
    private static final String URL = "https://raw.githubusercontent.com/mledoze/countries/master/countries.json";

    public List<CountryInfo> fetchAllData() {
        ResponseEntity<CountryInfo[]> forEntity = countryRestTemplate.getForEntity(URL, CountryInfo[].class);
        return Arrays.asList(Objects.requireNonNull(forEntity.getBody()));
    }
}
