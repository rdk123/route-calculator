package com.example.demo.controller;

import com.example.demo.model.RouteDTO;
import com.example.demo.service.RouteCalculatorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/routing")
@RequiredArgsConstructor
public class RouteCalculatorController {
    private final RouteCalculatorService routeCalculatorService;

    @GetMapping("/{origin}/{destination}")
    public ResponseEntity<RouteDTO> getRoute(@PathVariable("origin") String origin,
                                             @PathVariable("destination") String destination) {
        RouteDTO bestRoute = routeCalculatorService.getBestRoute(origin, destination);
        return ResponseEntity.ok(bestRoute);
    }
}
