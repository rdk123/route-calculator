package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
public class CountryApiConfiguration {
    @Value("#{T(java.time.Duration).parse('${country.api.read.timeout}')}")
    private Duration readTimeout;
    @Value("#{T(java.time.Duration).parse('${country.api.connect.timeout}')}")
    private Duration connectTimeout;

    @Bean
    public RestTemplate countryRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_PLAIN));
        messageConverters.add(converter);
        return restTemplateBuilder
                .setReadTimeout(readTimeout)
                .setConnectTimeout(connectTimeout)
                .messageConverters(messageConverters)
                .build();
    }

}
