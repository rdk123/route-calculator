package com.example.demo.service;

import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.NotFoundException;
import com.example.demo.model.CountryInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RoutesValidator {

    public void validate(CountryInfo origin, CountryInfo destination) {
        if (origin == null) {
            log.warn("[RoutesValidator] Origin country doesn't exist in dictionary");
            throw new NotFoundException("Origin country doesn't exist in dictionary");
        }
        if (destination == null) {
            log.warn("[RoutesValidator] Destination country doesn't exist in dictionary");
            throw new NotFoundException("Destination country don't have any borders");
        }
        if (origin.getBorders().isEmpty()) {
            log.warn("[RoutesValidator] Origin country: [{}] has no borders", origin);
            throw new BadRequestException("Origin country has no borders");
        }
        if (destination.getBorders().isEmpty()) {
            log.warn("[RoutesValidator] Destination country: [{}] has no borders", destination);
            throw new BadRequestException("Destination country has no borders");
        }
        if (!origin.getRegion().equals(destination.getRegion())) {
            log.warn("[RoutesValidator] Origin country: [{}] and destination country: [{}] don't have the same region", origin, destination);
            throw new BadRequestException("Destination country don't have the same region");
        }
    }
}
