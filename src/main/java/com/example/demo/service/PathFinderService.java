package com.example.demo.service;

import com.example.demo.exception.BadRequestException;
import com.example.demo.misc.Util;
import com.example.demo.model.CountryInfo;
import com.example.demo.model.RouteDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Service
@RequiredArgsConstructor
public class PathFinderService {
    private final List<CountryInfo> countriesInfo;

    public RouteDTO findShortestPath(String origin, String destination) {
        Map<String, Integer> countryCodeDictionary = getCountryCodeDictionary();
        List<List<Integer>> adj = getAdjacent(countryCodeDictionary);
        return getShortestRoute(adj, countryCodeDictionary.get(origin), countryCodeDictionary.get(destination), countryCodeDictionary);
    }

    private Map<String, Integer> getCountryCodeDictionary() {
        return IntStream
                .range(0, countriesInfo.size())
                .boxed()
                .collect(Collectors.toMap(i -> countriesInfo.get(i).getCountryCode(), i -> i));
    }

    private List<List<Integer>> getAdjacent(Map<String, Integer> countryCodeDictionary) {
        List<List<Integer>> adj =
                new ArrayList<>(countriesInfo.size());
        for (CountryInfo countryInfo : countriesInfo) {
            adj.add(countryInfo
                    .getBorders()
                    .stream()
                    .map(countryCodeDictionary::get)
                    .collect(Collectors.toList()));
        }
        return adj;
    }

    private static RouteDTO getShortestRoute(
            List<List<Integer>> adj,
            int origin, int destination, Map<String, Integer> countryCodeDictionary) {
        int[] pred = new int[countryCodeDictionary.size()];
        int[] dist = new int[countryCodeDictionary.size()];

        if (!BFS(adj, origin, destination, countryCodeDictionary.size(), pred, dist)) {
            log.warn("Given origin({}) and destination({}) not connected", origin, destination);
            throw new BadRequestException("Given origin and destination not connected");
        }

        LinkedList<Integer> path = new LinkedList<>();
        int crawl = destination;
        path.add(crawl);
        while (pred[crawl] != -1) {
            path.add(pred[crawl]);
            crawl = pred[crawl];
        }

        RouteDTO routeDTO = new RouteDTO();
        Collections.reverse(path);
        routeDTO.setRoute(path.stream().map(id -> Util.getKey(countryCodeDictionary, id)).collect(Collectors.toList()));
        return routeDTO;
    }

    // Info about BFS algorithm https://www.geeksforgeeks.org/breadth-first-search-or-bfs-for-a-graph/
    private static boolean BFS(List<List<Integer>> adj, int src,
                               int dest, int v, int[] pred, int[] dist) {
        LinkedList<Integer> queue = new LinkedList<>();

        boolean[] visited = new boolean[v];

        for (int i = 0; i < v; i++) {
            visited[i] = false;
            dist[i] = Integer.MAX_VALUE;
            pred[i] = -1;
        }

        visited[src] = true;
        dist[src] = 0;
        queue.add(src);

        while (!queue.isEmpty()) {
            int u = queue.remove();
            for (int i = 0; i < adj.get(u).size(); i++) {
                if (adj.get(u).get(i) != null && !visited[adj.get(u).get(i)]) {
                    visited[adj.get(u).get(i)] = true;
                    dist[adj.get(u).get(i)] = dist[u] + 1;
                    pred[adj.get(u).get(i)] = u;
                    queue.add(adj.get(u).get(i));

                    if (adj.get(u).get(i) == dest)
                        return true;
                }
            }
        }
        return false;
    }
}
